import cv2
import time


class Streamer:
    def __init__(self, config):
        self.config = config

    def __iter__(self):
        capture = cv2.VideoCapture(self.config.device)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.config.width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.config.height)
        capture.set(cv2.CAP_PROP_SATURATION, self.config.saturation)

        per = 1.0 / self.config.fps

        while True:
            rc, img = capture.read()
            if not rc:
                pass
            else:
                retval, buf = cv2.imencode(".jpg", img)
                yield buf.tostring()
                time.sleep(per)
