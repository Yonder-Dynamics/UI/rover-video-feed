import json

class Config():
    def __init__(self, **template):
        self.template = template

    def __call__(self, config):
        return self.build(config)

    def build(self, config):
        cfg = lambda: None
        for key, typecast in self.template.items():
            if key not in config:
                raise KeyError("{} not found in config".format(key))
            if not callable(typecast):
                raise ValueError("{} is not a valid type cast".format(key))
            setattr(cfg, key, typecast(config[key]))
        return cfg

    def build_from_file(self, file):
        with open(file) as src:
            config = json.load(src)
        return self.build(config)
