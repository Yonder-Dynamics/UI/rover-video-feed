#!/usr/bin/env python3
import argparse

import redis

from ConfigTemplate import Config
from MJPEGStream import Streamer


CONFIG_TEMPLATE = Config(
    redis=Config(
        host=str,
        port=int,
        name=str,
        topic=str,
    ),
    stream=Config(
        fps=float,
        device=int,
        width=int,
        height=int,
        saturation=float,
    ),
)


def stream_video(config):
    log_msg("Connecting to redis")

    client = redis.StrictRedis(host=config.redis.host, port=config.redis.port)
    client.client_setname(config.redis.name)

    try:
        for frame in Streamer(config.stream):
            client.publish(config.redis.topic, frame)
    except KeyboardInterrupt:
        pass


def build_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile")

    return parser


def log_msg(msg):
    print(">\t{}".format(msg))


if __name__ == "__main__":
    cmdline = build_parser().parse_args()
    config = CONFIG_TEMPLATE.build_from_file(cmdline.configfile)

    print("\n\n*****\n\n")
    log_msg("Starting video stream")
    stream_video(config)
    log_msg("Stopping video stream")
