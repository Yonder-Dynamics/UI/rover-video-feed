# MJPEG Video Streaming

Stream video as a series of `jpg` encoded images.

The current approach uses Redis topics to broadcast encoded frames from the
webcam.

The host computer must have the opencv and redis python modules installed. The
provided makefile resolves these dependencies using `Docker`, but the module
does not require it.

The executable takes the path to the config file as its only argument.