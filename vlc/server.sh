#!/bin/bash
DEVICE="/dev/video0"

PORT="8090"

cvlc v4l:// :v4l-vdev="${DEVICE}" --sout "#transcode{vcodec=VP80,vb=2000,scale=Auto,acodec=none,bframes=0}:http{mux=webm,dst=:${PORT}/}"
